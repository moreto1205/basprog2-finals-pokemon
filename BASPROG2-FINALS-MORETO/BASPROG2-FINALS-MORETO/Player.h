#pragma once
#include<string>
#include<time.h>

#include"Pokemon.h"
#include"Pokemon.cpp"
using namespace std;

class Player
{
public:
	Player();
	string name;

	int pokemonteamsize = 1;
	int starterchoice;
	int mainchoice;
	int movement;
	int pokecenterchoice;
	int pokemartchoice;
	int battlechoice;
	int x;
	int y;
	int towncheck;
	int pokeballs = 10;
	int pokeballpurchase;
	int pokeyen = 1000;

	// Pokemon Party
	Pokemon* pokemonParty[6];
	int size = 0;

	//

	string starter[3]{ "BULBASAUR", "SQUIRTLE", "CHARMANDER" };
	string pokemonteam[6];

	Player(string name);
	void intro(string name);

	void gameplay(int x, int y, int pokemonteamsize, int towncheck);

};
