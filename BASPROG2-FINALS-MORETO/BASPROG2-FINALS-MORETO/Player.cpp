#include "Player.h"
#include "Pokemon.h"
#include <iostream>
#include <string>

using namespace std;


int randDamage = rand() % 15 + 10;
int randXP = rand() % 15 + 5;
int randLevel = rand() % 20 + 1;
int randencounterchance = rand() % 25 + 10;
int randcatchchance = rand() % 15 + 10;
int randpokeyen = rand() % 100 + 5;
int randHP = rand() % 100 + 1;
int maxhp = randHP;

Pokemon* wild = new Pokemon(randHP, maxhp, randDamage, randLevel, randXP, 50);

Player::Player()
{
	this->name = "";
};

Player::Player(string name)
{
	this->name = name;
};

void Player::intro(string name)
{
	cout << "HELLO TRAINER, WHAT IS YOUR NAME?" << endl;
	cin >> this->name;
	cout << R"(I HAVE THREE POKEMON FOR YOU TO CHOOSE FROM 
				0 - BULBASAUR
				1 - SQUIRTLE
				2 - CHARMANDER)" << endl;
	cin >> starterchoice;
	cout << "YOU CHOSE " << starter[starterchoice] << " AS YOUR STARTING POKEMON" << endl;
	cout << "NOW ENTER THE WORLD OF POKEMON WITH YOU NEW PARTNER AND EB THE VERY BEST" << endl;
	system("pause");
	system("cls");

	wild->starter(NULL);
};

void Player::gameplay(int x, int y, int pokemonteamsize, int towncheck)
{
	int randDamage = rand() % 15 + 10;
	int randXP = rand() % 15 + 5;
	int randLevel = rand() % 20 + 1;
	int randencounterchance = rand() % 15 + 1;
	int randcatchchance = rand() % 15 + 10;
	int randpokeyen = rand() % 100 + 5;
	int attackchance = rand() % 10 + 1;

	cout << R"(WHAT WOULD YOU LIKE TO DO?
				1 - MOVE
				2 - POKEMON
				3 - POKEMON CENTER
				4 - POKEMON MART )" << endl;

	while (true)
	{
		cin >> mainchoice;

		switch (mainchoice)
		{
		case 1:

			cout << R"( SELECT WHERE TO GO
						1 - FORWARD
						2 - BACKWARD
						3 - LEFT
						4 - RIGHT)" << endl;
			cin >> movement;

			switch (movement)
			{
			case 1:
			{
				y++;
				{
					if (y >= -1 && y <= 1 && x >= -2 && x <= 2)
					{
						cout << "PALLET TOWN" << endl;
						towncheck = 1;
					}
					else if (y > 2 && y <= 5 && x >= -2 && x <= 2)
					{
						cout << "ROUTE 1" << endl;
						towncheck = 0;
					}
					else if (y > 5 && x > 3)
					{
						cout << "PEWTER CITY" << endl;
						towncheck = 1;
					}
					cout << "(" << x << "," << y << ")" << endl;
				}
				break;
			}
			case 2:
			{
				y--;
				{
					if (y >= -1 && y <= 1 && x >= -2 && x <= 2)
					{
						cout << "PALLET TOWN" << endl;
						towncheck = 1;
					}
					else if (y < -1 && y >= -5 && x >= -2 && x <= 2)
					{
						cout << "ROUTE 21" << endl;
						towncheck = 0;
					}
					else if (y < -5 && x < -3)
					{
						cout << "UNKNOWN LOCATION" << endl;
						towncheck = 0;
					}
					cout << "(" << x << "," << y << ")" << endl;
				}
				break;
			}
			case 3:
			{
				x--;
				{
					if (y >= -1 && y <= 1 && x >= -2 && x <= 2)
					{
						cout << "PALLET TOWN" << endl;
						towncheck = 1;
					}
					else if (y > -1 && y <= 1 && x < -2 && x >= -5)
					{
						cout << "ROUTE 22 " << endl;
						towncheck = 0;
					}
					else if (y < 3 && x < -5)
					{
						cout << "UNKNOWN LOCATION" << endl;
						towncheck = 0;
					}
					cout << "(" << x << "," << y << ")" << endl;

				}
				break;
			}
			case 4:
			{
				x++;
				{
					if (y >= -1 && y <= 1 && x >= -2 && x <= 2)
					{
						cout << "PALLET TOWN" << endl;
						towncheck = 1;
					}
					else if (y > 2 && y <= 4 && x > 2 && x <= 4)
					{
						cout << "VERMILLION CITY" << endl;
						towncheck = 1;
					}
					else if (y > 4 && x > 4)
					{
						cout << "UNKNOWN LOCATION" << endl;
						towncheck = 0;
					}
					cout << "(" << x << "," << y << ")" << endl;
				}
				break;

			}
			break;
			}
		case 2:

			wild->stats(randHP, maxhp, randDamage, randXP, 50, randLevel, 5, randpokeyen, 5);
			/*
			int random = rand() % 15;
			Pokemon* newPokemon = new Pokemon(wildpokemon[randencounterchance], randHP, randDamage, randxp, 100, randlevel, pokemonteamsize,
			randpokeyen, 5);

			//if captured
			pokemonParty[size]->CopyPokemon(newPokemon);
			size++;

			*/

			break;

		case 3:

			cout << "WELCOME TO THE POKECENTER!" << endl;
			cout << "WOULD YOU LIKE TO HEAL YOUR POKEMON?" << endl;
			cout << R"( 1- YES 
						2 - NO )" << endl;
			cin >> pokecenterchoice;

			switch (pokecenterchoice)
			{
			case 1:
				cout << "ALL OF YOUR POKEMON HAVE BEEN HEALED" << endl;


				for (int i = 1; i < pokemonteamsize; i++)
				{
					//pokemonParty[i].hp = max hp
					wild->baseHP = wild->maxhp;
				}
				cout << "HOPE TO SEE YOU AGAIN SOON" << endl;
				break;

			case 2:
				cout << "THANK YOU FOR VISITING HOPE TO SEE YOU AGAIN SOON" << endl;

				break;
			};
			break;

		case 4:
			cout << "WELCOME TO THE POKEMART!" << endl;
			cout << "WOULD LIKE TO PURCHASE POKEBALLS" << endl;
			cout << R"( 1- YES 
						2 - NO )" << endl;

			cin >> pokemartchoice;
			switch (pokemartchoice)
			{
			case 1:
				cout << "HOW MANY POKEBALLS WOULD YOU LIKE TO BUY?" << endl;
				cin >> pokeballpurchase;
				cout << "YOU HAVE PURCHASED " << pokeballpurchase << "POKEBALLS" << endl;
				cout << "THAT WILL BE " << (pokeballpurchase * 100) << "POKEYEN" << endl;
				pokeyen = pokeyen - (pokeballpurchase * 100);
				pokeballs = pokeballs + pokeballpurchase;
				cout << "WE HOPE TO SEE YOU AGAIN" << endl;

				if (pokeyen < (pokeballpurchase * 100))
				{
					cout << "YOU DONT HAVE ENOUGH MONEY TO BUY THAT AMOUNT PLEASE INPUT A PROPER AMOUNT" << endl;
				}

					break;

			case 2:
				cout << "WE HOPE TO SEE YOU AGAIN" << endl;

					break;
			}

			break;

		}
		system("pause");
		system("cls");

		if (towncheck == 1)
		{
			cout << R"(WHAT WOULD YOU LIKE TO DO?
				1 - MOVE
				2 - POKEMON
				3 - POKEMON CENTER
				4 - POKEMON MART )" << endl;

		}

		else if (towncheck == 0)
		{
			if (randencounterchance > 15)
			{
				wild->encounter(wild->randencounterchance, wild->randHP, wild->randLevel, wild->randDamage, randXP, 50);
				cin >> battlechoice;

				switch (battlechoice)
				{
				case 1:

					if (attackchance < 9)
					{
						wild->battle(50, randDamage, randencounterchance, 0, 50, 1, randpokeyen);
						break;
					}
					else if (attackchance >= 9)
					{
						cout << "YOU MISSED YOUR ATTACK" << endl;
						break;
					}

				case 2:

					wild->catching(randcatchchance, 1, randencounterchance, pokeballs, pokeyen);
					break;

				case 3:
					cout << "YOU RAN AWAY" << endl;
					break;
				}
			}

			cout << R"(WHAT WOULD YOU LIKE TO DO?
				1 - MOVE
				2 - POKEMON )" << endl;
		}
	}


}