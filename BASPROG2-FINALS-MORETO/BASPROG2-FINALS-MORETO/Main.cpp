#include <iostream>
#include <string>
#include <time.h>
#include "Player.h"
#include "Pokemon.h"
using namespace std;

void textart()
{
	cout << R"(
__________________   ____  __.___________   _____   ________    _______            
\______   \_____  \ |    |/ _|\_   _____/  /     \  \_____  \   \      \           
 |     ___//   |   \|      <   |    __)_  /  \ /  \  /   |   \  /   |   \          
 |    |   /    |    \    |  \  |        \/    Y    \/    |    \/    |    \         
 |____|   \_______  /____|__ \/_______  /\____|__  /\_______  /\____|__  /         
                  \/        \/        \/         \/         \/         \/          
  _________.___   _____   ____ ___.____       ________________________ __________  
 /   _____/|   | /     \ |    |   \    |     /  _  \__    ___/\_____  \\______   \ 
 \_____  \ |   |/  \ /  \|    |   /    |    /  /_\  \|    |    /   |   \|       _/ 
 /        \|   /    Y    \    |  /|    |___/    |    \    |   /    |    \    |   \ 
/_______  /|___\____|__  /______/ |_______ \____|__  /____|   \_______  /____|_  / 
        \/             \/                 \/       \/                 \/       \/)" << endl;
}

void main()
{
	srand(time(NULL));
	int randDamage = rand() % 15 + 10;
	int randXP = rand() % 15 + 5;
	int randLevel = rand() % 20 + 1;
	int randencounterchance = rand() % 15 + 1;
	int randcatchchance = rand() % 15 + 10;
	int randpokeyen = rand() % 100 + 5;
	Player* master = new Player("");
	Pokemon* wild = new Pokemon(50, 0, randDamage, randLevel, 0, 50);
	textart();
	system("pause");
	system("cls");
	master->intro("");
	master->gameplay(0, 0, 1, NULL);
}